.. index::
   single: Estandares y estructura

Estandares y estructura del proyecto
====================================

Como pueden ver en el repositorio mencionado en el capitulo anterior, hay un archivo **index.rst** y una carpeta **documentando** .

*	Hay un carpeta por cada seccion de la documentacion como pueden ver por ejemplo **documentando**
*	La carpeta contiene un **map.rst.inc** y un **index.rst** obligatoriamente
*	En **map.rst.inc**	hay un mapeo de todos los archivos que contiene la carpeta

	.. literalinclude:: map.rst.inc
		:language: rst

*	En **index.rst** hay un **toctree** el cual sirve para "indexar" los capitulos de la seccion, y se incluye el archivo **map.rst.inc** el cual contiene links a los capitulos.
 	
 	.. literalinclude:: index.rst
 		:language: rst

*	En cada capitulo de la seccion lo primero que se tiene que definir es el **index**, para decirle a sphinx que es un indice

	.. code-block:: rst
	
	    .. index::
   	       single: Empezando a documentar


    Esto crea un indice con el titulo "Empezando a documentar"

*	Por ultimo en el **index.rst** principal se incluyen las secciones

	.. literalinclude:: ../index.rst
		:language: rst

Como pueden ver se incluye la seccion por ejemplo **Aprendiendo a documentar** al proyecto de la siguiente manera

Se le dice a Sphinx donde buscar los indices linkeando un toctree al index de la seccion en el caso de documentacion es ``documentando/index``

Luego se incluye el **map.rst.inc** el cual contiene links a los distintos capitulos de la seccion ``.. include:: /documentando/map.rst.inc``		

.. note::
	Como pueden ver organizar las secciones en carpetas es mucho mas util ya que lo unico que hay que hacer es agregar el toctree y los indices de la seccion, al index principal, "Es mas modular"