.. index::
   single: Creando un proyecto de documentacion

Creando un proyecto de documentacion
====================================

En este capitulo se explica como crear un proyecto de documentacion desde cero.

.. attention::
	Si no les intereza como crear un proyecto, sino simplemente documentar
	vayan al capitulo:
	:doc:`/documentando/empezando`

Librerias necesarias para crear un proyecto nuevo:
--------------------------------------------------

*	Sphix
*	VirtualEnvironment
*	Pip
*	Easy_install
 	

Instalando las librerias
------------------------

Lo que tienen que hacer es lo siguiente:

1. Bajar la libreria easy_install de python

   .. code-block:: bash
   
       	wget https://bootstrap.pypa.io/ez_setup.py -O - | python

2. Instalar pip

	.. code-block:: bash
	
	    pip install virtualenv

3. Una vez instalado el virtualenv, creamos un entorno virtual donde vamos a descargar las librerias necesarias ( en este caso solamente sphinx )

	.. code-block:: bash
	
	    virtualenv nombreEnterno

4. Activamos el entorno virtual

	.. code-block:: bash
	
		
		#Entramos al entorno
	    cd nombreEntorno

	    #Lo activamos
	    source bin/activate

	Se van a dar cuenta que esta activado porque aparece "(nombreEntorno) ..."

5. Le instalamos la libreria Sphinx al entorno que creamos

	.. code-block:: bash
	
	    pip install sphinx

Con estos 5 pasos ya vamos a tener todas las dependencias de la libreria y la libreria instaladas en el entorno virtual


Creando el proyecto de documentacion
------------------------------------

Estando dentro del entorno, van a cualquier carpeta del sistema de archivos por ejemplo el home

	.. code-block:: bash
	
	    cd ~

Crean una carpeta nueva

	.. code-block:: bash
	
	    mkdir ejemploDocs

Lo unico que hay que ingresar ahora es el siguiente comando para crear el proyecto

	.. code-block:: bash
	
	    sphinx-quickstart

El comando los va a llevar a un wizard donde les va a hacer algunas preguntas para crear el proyecto

Las unicas que tienen que modificar son:

*	Separate source and build directories (y/N) [n] , pongan "y"
*	Name prefix for templates and static dir [_] , pongan "n" si es que quieren usar el template de readthedocs
*	Would you like to automatically insert ... , pongan "y"
 	
Una vez que terminen de responder las preguntas se les genera una estructura de directorios

Makefile ,build, make.bat, source

Lo que pueden hacer es borrar **make.bat** si quieren .., luego borrar el directorio **build**
y copiar todo lo que contenga el directorio **source** un nivel mas arriba "../".

Entonces les va a quedar solamente Makefile y los archivos que tenia el directorio source.

Ya con esto pueden empezar a documentar editando el archivo index.rst