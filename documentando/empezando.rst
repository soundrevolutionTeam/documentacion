.. index::
   single: Empezando a documentar

Empezando a documentar
======================

.. important:: 
	Es importante que lean desde este capitulo en adelante ya que se definen estandares que vamos a usar para 
	documentar.

Para poder documentar mas facil es recomendable instalar el siguiente plugin de sublime-text:

`Plugin`_ 

.. _Plugin: https://sublime.wbond.net/packages/Restructured%20Text%20(RST)%20Snippets

En este link podran encontrar bastante documentacion del formato "Sphinx"

`Tutorial`_ 

.. _Tutorial: http://www.youtube.com/watch?v=otM_tjIi_vY

Este ultimo es un video que muestra como usar el plugin, muy util.

.. tip:: 
	El documento tiene que estar en sintaxis restructuredText 
	( lo cambian con CTRL+ SHIFT + P )
	escriben "restruc" y ya les salta la opcion
	para usar los "shortcuts", deben apretar **TAB** luego de una palabra clave por ejemplo **h1 + TAB**	
	
Tutoriales sobre el formato del codigo:

http://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html

http://sphinx-doc.org/rest.html

http://documentation-style-guide-sphinx.readthedocs.org/en/latest/style-guide.html

Una vez que hayan visto la documentacion y el video ya pueden empezar a documentar basicamente.

Repositorio
-----------

El repositorio que vamos a usar es el siguiente

``https://bitbucket.org/soundrevolutionTeam/documentacion``

.. attention:: Por favor no anden pasando el repositorio ya que es de acceso publico, ya que sino readthedocs no lo podria descargar

Forma de trabajar
-----------------

Se hacen un pull del repositorio en cualquier lugar del sistema de archivos, agregan lo que quieran, hacen un commit luego un push, y automaticamente readthedocs construye la nueva documentacion, esto es posible gracias a un hook entre bitbucket y readthedocs que le avisa cuando se hace un push.


En el proximo capitulo vamos a explicar los estandares del proyecto de documentacion mencionado en el repo.