.. SoundRevolution Documentacion documentation master file, created by
   sphinx-quickstart on Sun Jul 13 03:07:40 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SoundRevolution
===============

Bienvenidos a la documentacion para el proyecto, aqui encontraran documentacion tanto para el proyecto como tips y ayudas nuestros.

.. toctree::
   :hidden:

   documentando/index

Aprendiendo a documentar
------------------------

.. include:: /documentando/map.rst.inc

Bundles
===============

Documentacion de los diferentes bundles que vamos a usar en el proyecto.

.. toctree::
   :hidden:

   bundles/index

Bundles de terceros
------------------------

.. include:: /bundles/map.rst.inc


Indice
================
* :ref:`genindex`

