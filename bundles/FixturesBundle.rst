.. index::
   single: Doctrine-fixtures-bundle

************************
Doctrine-fixtures-bundle
************************

Introducción
============

En casi todos los sistemas informáticos, cuando nos disponemos a poner en marcha nuestro sistema se necesitan datos iniciales, como pueden ser, por ejemplo un usuario administrador, el primer y único usuario o carga inicial en tablas de referencia. Para realizar realizar esta tarea Symfony 1 contaba con fixtures los cuales contenían datos que symfony cargaba al inicializar la BD, Symfony2 no cuenta con algún método de carga inicial de datos por defecto, para poder contar con esto es necesario agregar un bundle al proyecto, Doctrine-fixtures-bundle se encarga de cargar datos en la BD con unos scripts que debemos armar.


Instalación
===========

Mediante Composer
-----------------

1- Se edita el archivo composer.json, y se agrega la linea:

.. code-block:: json

  "doctrine/doctrine-fixtures-bundle": "2.2.*" en el array de require

  

2- Se actualiza el composer junto con todas sus dependencias

3- Se edita el archivo appKernel.php y en el array de bundles se agrega la linea:
  
  .. code-block:: php

    new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),


Uso
===

1- Se agrega un archivo en la carpeta DataFixtures/ORM del bundle, por ejemplo LoadUserData.php

2- En el nuevo archivo php se agrega la siguiente estructura:

.. code-block:: php

  namespace creador\bundleBundle\DataFixtures\ORM;

  use Doctrine\Common\DataFixtures\FixtureInterface;
  use Doctrine\Common\Persistence\ObjectManager;
  use creador\bundleBundle\Entity\User;

  class LoadUserData implements FixtureInterface
  {
      /**
       * {@inheritDoc}
       */
      public function load(ObjectManager $manager)
      {
          $userAdmin = new User();
          $userAdmin->setUsername('admin');
          $userAdmin->setPassword('test');

          $manager->persist($userAdmin);
          $manager->flush();
      }
  }



Como podemos ver, en el codigo anterior, estamos creando un usuario "Admin" y lo estamos agregando en la BD

3- Ejecutamos el fixtures

.. code-block:: bash

  php app/console doctrine:fixtures:load

  
Documentacion oficial
=====================

http://symfony.com/doc/current/bundles/DoctrineFixturesBundle/index.html
