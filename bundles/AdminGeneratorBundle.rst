************************
Admin-generator-bundle
************************

Introduccion
============

Es bundle nos permite automatizar la creacion de la vista con su respectivo controlador, con ABM, busqueda y listado de entidades en los bundles que tenemos en nuestro sistema.
A diferencia del SONATA admin generator, este se parece mas al generator de symfony 1, ya que su principal configuracion se encuentra en un archivo yml.


Instalacion
===========

Mediante Composer
-----------------

1- Se edita el archivo composer.json, y se agregna las lineas:

.. code-block:: json

    "require": {
        "cedriclombardot/admingenerator-generator-bundle": "dev-master"
    }

.. code-block:: json

    "config": {
        "component-dir": "web/components"
    }


2- Se actualiza el composer junto con todas sus dependencias

3- Se edita el archivo appKernel.php y en el array de bundles se agrega la linea:
  
.. code-block:: php
    
      new Admingenerator\GeneratorBundle\AdmingeneratorGeneratorBundle()

4- se edita el archivo app/config/config.yml para agregar:

.. code-block:: yml

    admingenerator_generator:
        # choose  and enable at least one
        use_propel:           true
        use_doctrine_orm:     true
        use_doctrine_odm:     false


Crear un generator
==================

Para crear un generator se necesita de un bundle el cual contenga la entidad en cuestion(el ABM sera de esta entidad), el admin generator puede crear un bundle junto con el generator o agregarlo a un bundle ya existente.


Creando un bundle
-----------------

.. code-block:: console

    php app/console admin:generate-bundle


Agregando a un bundle existente
-------------------------------

.. code-block:: console

    php app/console admin:generate-admin


Al ejecutar cualquiera de los 2 comandos, la consola pedira algunos datos, como nombre del bundle y nombre de la entidad para poder crear el generator.


.. attention:: 
  * Cuando nos pregunte por el prefix, no se debe usar caracteres especiales ya que ocurren problemas.
  * El archivo yml creado se llamara: "prefix-generator.yml" (suma el -generator.yml al prefix).
  * Al crear un bundle, tambien nos pedira una entidad, esta debemos agregarla luego, si es que no existe.


Personalizando
============

Una vez que tengamos creado el generator, vamos a poder acceder a nuestro modulo, pero este tendrá una configuración por defecto, la cual es muy probable que tengamos que modificar.
Para poder lograr esto tenemos que modificar el archivo prefix-generator.yml que se encuentra nuestro bundle/Resources/config.

Modificando el template base
-----------------

Los templates base son los equivalentes a los layouts en symfony 1, contienen la vista básica para el modulo y se pueden compartir con otros módulos, esto evita tener que repetir código.

Dentro del archivo generator, al nivel de generator y param, y entra ambos colocamos la entrada base_admin_template la cual contiene la ruta del twig que se usara de base.

Ejemplo:

.. code-block:: yml

    generator: admingenerator.generator.doctrine
    base_admin_template: SystemSystemBundle::base_new_admin.html.twig


Cargando un css para nuestro modulo mediante generator
-----------------

Muchas veces queremos que nuestro modulo cargue un css adicional, para lograr esto sin tener que modificar el twig o poniendo el css en todos los módulos, se le puede poner al generator.yml que cargue css adicionales.

Dentro del archivo generator, un nivel dentro de params, colocamos la entrada stylesheets, donde cargaremos los distintos css.

Ejemplo:

.. code-block:: yml

    generator: admingenerator.generator.doctrine
    base_admin_template: SystemSystemBundle::base_new_admin.html.twig
    params:
        model: System\SecurityBundle\Entity\User
        stylesheets:
          - bundles/systemsystem/css/admin.css


Cargando catalogo i18n
-----------------

En symfony2 existen catálogos de i18n, son conjuntos de textos en un idioma que se traducen a otro idioma, es muy probable que queramos que nuestro modulo tenga uno de estos catálogos.

Dentro del archivo generator, dentro de params, colocamos la entrada i18n_catalog.

Ejemplo:

.. code-block:: yml

    bundle_name: SecurityBundle
    i18n_catalog: messages


Esto carga los catálogos messages para el idioma seleccionado.


Documentacion oficial
=====================

http://symfony2admingenerator.org/documentation/quick-start.htm

https://github.com/symfony2admingenerator/AdmingeneratorGeneratorBundle/blob/master/Resources/doc/documentation.md#table-of-contents
